{
  lib,
  buildGoModule,
  fetchFromGitHub,
  pkgs,
  makeWrapper,
  ...
}:
buildGoModule rec {
  pname = "k0s";
  version = "1.27.4+k0s.0";
  subPackages = [ "." ];
  nativeBuildInputs = [ makeWrapper ];

  src = fetchFromGitHub {
    owner = "k0sproject";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-NcMsZUOpzmo/wqAN7h/4RTYq9rHdh8neFPoPGev6ERY=";
  };

  vendorSha256 = "sha256-HNV2snSiWr1LROudT2BUkrmvii+l7rtEU44+oEGswQA=";

  ldflags = [
    "-s"
    "-w"
    "-X github.com/k0sproject/k0s/pkg/build.Version=${version}"
  ];

  preBuild = ''
    ${pkgs.go-bindata}/bin/go-bindata -o static/zz_generated_assets.go -pkg static -prefix static static/manifests/... static/misc/...
    mkdir -p embedded-bins/staging/linux/bin
    ${pkgs.go}/bin/go run -tags=hack hack/gen-bindata/cmd/main.go -o bindata_linux -pkg assets -gofile pkg/assets/zz_generated_offsets_linu.go -prefix embedded-bins/staging/linux/ embedded-bins/staging/linux/bin
  '';
  postFixup = ''
    wrapProgram $out/bin/k0s \
      --set PATH ${lib.makeBinPath [
        pkgs.etcd_3_5
        pkgs.kubernetes
      ]}
  '';
}
