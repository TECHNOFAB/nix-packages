{
  inputs,
  lib,
  stdenv,
  writeTextFile,
  pkgs,
  runtimeShell,
}: let
  name = "gitlab-terraform";
  libidn2 = pkgs.callPackage "${inputs.nixpkgs}/pkgs/development/libraries/libidn2" {};
  runtimeInputs = [
    pkgs.terraform
    pkgs.gnused
    pkgs.gawk
    pkgs.jq
    (libidn2.overrideAttrs (old: {
      # idn2: libiconv required for non-UTF-8 character encoding: ANSI_X3.4-1968
      buildInputs = old.buildInputs ++ [pkgs.libiconv];
    }))
  ];
in
  writeTextFile {
    inherit name;
    executable = true;
    destination = "/bin/${name}";
    allowSubstitutes = true;
    preferLocalBuild = false;
    text =
      ''
        #!${runtimeShell}
        set -o errexit
        set -o pipefail
      ''
      + lib.optionalString (runtimeInputs != []) ''

        export PATH="${lib.makeBinPath runtimeInputs}:$PATH"
      ''
      + ''

        ${builtins.readFile ./gitlab_terraform.sh}
      '';

    meta.mainProgram = name;
  }
