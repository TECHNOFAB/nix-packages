{
  lib,
  mkYarnPackage,
  fetchYarnDeps,
  fetchFromGitHub,
  ...
}:
mkYarnPackage rec {
  pname = "camouflage";
  version = "0.13.0";

  src = fetchFromGitHub {
    owner = "testinggospels";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-es3gLGRD6ocv9ghz/rwrWSmz422zqiYgOQ8zj0er8Ec=";
  };
  offlineCache = fetchYarnDeps {
    yarnLock = "${src}/yarn.lock";
    sha256 = "sha256-DUk7+lOD0sBTCUw0PhB32hdCfZGFdR2quA8u7M95b10=";
  };

  buildPhase = ''
    runHook preBuild

    yarn run build

    runHook postBuild
  '';
}
