{
  lib,
  fetchFromGitHub,
  buildDartApplication,
  ...
}:
buildDartApplication rec {
  pname = "protoc-gen-dart";
  version = "21.0.2";
  src =
    fetchFromGitHub {
      owner = "google";
      repo = "protobuf.dart";
      rev = "protoc_plugin-v${version}";
      hash = "sha256-HahN4MOV8Rk0HBwRoxO2VvMRYfXr0MFsPvkw79ZKzDE=";
    };

  sourceRoot = "${src}/protoc_plugin";

  pubspecLockFile = ./pubspec.lock;
  vendorHash = "sha256-Doile6n0eAXpoJ7969E28AqysC9iAk6kNkEAT1niY3Y=";
}
