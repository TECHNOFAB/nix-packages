{
  lib,
  fetchFromGitHub,
  buildGoModule,
  fetchYarnDeps,
  fixup_yarn_lock,
  nodejs,
  yarn,
  mkYarnPackage,
  makeWrapper,
  go-rice,
  sassc,
  ...
}:
buildGoModule rec {
  pname = "statping-ng";
  version = "0.90.80";
  src = fetchFromGitHub {
    owner = pname;
    repo = pname;
    rev = "v${version}";
    hash = "sha256-zNlF8mpRrNsyhWYCUiHiZlBL0mHvJypy9ZtqwRvWqDo=";
  };

  offlineCache = fetchYarnDeps {
    yarnLock = src + "/frontend/yarn.lock";
    hash = "sha256-e8GyKIJ0RopRliVMVrY8eEd6Qx/gTKbW3biPCSqbRrQ=";
  };

  CGO_ENABLED = 1;
  vendorSha256 = "sha256-r9hSIFWGA1zXx9bLyaEz3gKMSPNHnc6cPIWysgpoUo8=";

  tags = [
    "netgo"
  ];

  ldflags = [
    "-s"
    "-w"
    "-X main.VERSION=${version} -X main.COMMIT=${version}"
  ];

  subPackages = ["cmd"];

  # cp -r ${frontend}/libexec/statping/deps/statping/dist source/dist
  preBuild = ''
    export HOME=$TEMPDIR

    pushd frontend
      yarn config --offline set yarn-offline-mirror ${offlineCache}
      fixup_yarn_lock yarn.lock

      yarn --offline install
      NODE_OPTIONS='--openssl-legacy-provider' NODE_ENV=production $(yarn bin webpack) --mode production
    popd

    cp -r frontend/dist source/dist

    pushd source
      rice embed-go
    popd
  '';

  postInstall = ''
    mv $out/bin/cmd $out/bin/statping-ng
    wrapProgram $out/bin/statping-ng --prefix PATH : ${lib.makeBinPath [sassc]}
  '';

  nativeBuildInputs = [
    fixup_yarn_lock
    makeWrapper
    nodejs
    yarn
    go-rice
  ];

  doCheck = false;

  meta = {
    description = "A Status Page for monitoring your websites and applications with beautiful graphs, analytics, and plugins";
    homepage = "https://statping-ng.github.io";
    license = lib.licenses.gpl3;
  };
}
