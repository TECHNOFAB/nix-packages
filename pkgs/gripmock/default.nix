{
  pkgs,
  fetchFromGitHub,
  ...
}:
pkgs.buildGoModule rec {
  pname = "gripmock";
  version = "1.12";

  src = fetchFromGitHub {
    owner = "tokopedia";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-BzTyxYNxAYicaXpwkW9YNqCeyQ5hiHx0JsBenheduXA=";
  };

  ldflags = ["-s" "-w"];

  vendorSha256 = "sha256-Lqs1DeN2MQPHQxkwmr/8OSOLF6g3cHbpdx0XcBlwYjM=";
  subPackages = ["gripmock.go"];

  doCheck = false;

  meta = {
    broken = true;
  };
}
