{
  inputs,
  system ? builtins.currentSystem,
  pkgs ? import <nixpkgs> {inherit system;},
}: rec {
  gitlab-terraform = pkgs.callPackage ./pkgs/gitlab-terraform {inherit inputs;};
  gripmock = pkgs.callPackage ./pkgs/gripmock {inherit inputs;};
  camouflage = pkgs.callPackage ./pkgs/camouflage {inherit inputs;};
  protoc-gen-dart = pkgs.callPackage ./pkgs/protoc-gen-dart {inherit inputs;};
  k0s = pkgs.callPackage ./pkgs/k0s {inherit inputs;};
  statping-ng = pkgs.callPackage ./pkgs/statping-ng {inherit inputs;};
}
